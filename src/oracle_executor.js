const oracledb = require("oracledb")
const fs = require("fs")
const config = require('./config.js')
const async = require('async')
const colors = require('colors')
const { error } = require("console")

oracledb.outFormat = oracledb.OUT_FORMAT_OBJECT

/**
 * 从schema.sql文件中获取sql
 */
function importSql() {
    const path = fs.realpathSync('./src/schema.sql')
    const content = fs.readFileSync(path).toString()
    if (!content) {
        throw new Error('未获取到待执行sql')
    }
    const arr = content.split(';')
    const sqlArr = []
    arr.forEach(e => {
        if (e) {
            sqlArr.push(e)
        }
    })
    return sqlArr
}

/**
 * 根据config，获取oracledb连接
 */
function getConnectionPromises() {
    if (!config.oracle || config.oracle.length === 0) {
        throw new Error('未配置Oracle数据库连接')
    }
    console.info('\r\n待执行数据库连接如下：')
    return config.oracle.map(async e => {
        // 根据active状态，获取数据库连接
        if (e.active) {
            console.info(e)
            const connection = await oracledb.getConnection({
                user: e.user,
                password: e.password,
                connectString: `${e.host}:${e.port}/${e.dbName}`
            })
            // 设置tag，方便识别
            connection.tag = e.tag
            return connection
        }
    })
}

/**
 * 批量执行sql
 */
function execute() {
    const sqlArr = importSql()
    console.info(`\r\n本次执行sql数量：${sqlArr.length}`)
    console.info(`本次待执行sql：\r\n${sqlArr}`)
    let promises = getConnectionPromises()

    for (var promise of promises) {
        promise.then((connection) => {
            async.eachSeries(sqlArr, (sql, callback) => {
                if (connection) {
                    connection.execute(sql)
                        .then(() => callback())
                        .catch(err => {
                            console.error(`数据库${connection.tag}执行时遇到异常！`.red)
                            console.error(`异常SQL：${sql}`)
                            console.error(err)
                            callback(false)
                        })
                }
            })
                .finally(() => console.log(`数据库${connection.tag}执行完毕`.green))
        })
    }
}

var executor = {
    execute: execute
}

module.exports = executor