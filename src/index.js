const oracleExecutor = require('./oracle_executor.js')
const colors = require('colors')

const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
})

// oracleExecutor.execute()

readline.question('FBI Warning：请一定确认好后，再执行数据库脚本！！！\n确认执行吗？(yes/no): '.yellow, name => {
    readline.close()
    if (name === 'yes') {
        oracleExecutor.execute()
    }
})