# sql-batch-executor

#### 介绍
一个简单的sql批量执行助手


#### 安装教程

1.  安装node环境，本程序基于node v14.16.0开发
2.  安装npm
3.  npm install

#### 使用说明

1.  在schema.sql中编写需要执行sql，每句sql注意以;分隔
2.  在config.js中oracle数组中，添加需要执行的数据库信息

    例：
    ```
    {
        // 名称
        tag: 'dev',
        host: '127.0.0.1,
        port: 1521,
        // 数据库名
        dbName: 'dev',
        user: 'dev',
        password: 'dev123',
        // 是否激活，激活则会执行sql，不激活不执行
        active: true
    }
    ```
3.  命令行执行node .\src\index.js